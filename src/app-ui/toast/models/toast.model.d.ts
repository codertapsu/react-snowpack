import { ToastConfig } from './toast-config.model';
import { ToastType } from './toast-type.model';

export interface Toast {
  id?: string;
  type: ToastType;
  message: string;
  config?: ToastConfig;
}
