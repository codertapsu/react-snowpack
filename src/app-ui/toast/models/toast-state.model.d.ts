import { Toast } from './toast.model';

export interface ToastState {
  items: Toast[];
  addToast?: (toast: Toast) => string;
  removeToast?: (id: string) => void;
}
