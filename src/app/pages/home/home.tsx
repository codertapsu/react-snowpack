import React, { FormEvent, useEffect, useRef, useState } from 'react';

import { useHttpClient } from '@contexts/http-client/http-client.context';
import { useOnClickOutside } from '@hook/use-click-outside';

import styles from './home.module.css';
import { RequestShape } from './models/request.model';

function HomePage() {
  const modal = useRef(null);
  const [dataTable, setDataTable] = useState<RequestShape[]>([]);
  const [searchText, setSearchText] = useState<string>('');

  const ref = useRef();
  const [isModalOpen, setModalOpen] = useState(false);
  useOnClickOutside(ref, () => setModalOpen(false));

  // const { execute, status, value, error } = useAsync(myFunction, false);

  const httpClient = useHttpClient();

  const getDataTable = () => {
    httpClient
      .post<{ list: RequestShape[] }>('request-form/grid', {
        status: undefined,
        textSearch: '',
      })
      .then(gridData => {
        console.log(gridData);
        setDataTable(gridData.list || []);
      })
      .catch(error => {
        console.log(error);
      });
  };

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const formData = new FormData(event.currentTarget);

    // login(formData.get('email') as string, formData.get('password') as string);
  };

  const handleChange = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const formData = new FormData(event.currentTarget);
    console.log([...formData.values()]);

    // login(formData.get('email') as string, formData.get('password') as string);
  };

  const updateSearchText = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchText(e.target.value);
  };

  const uniqeKey = '1';

  useEffect(() => {
    getDataTable();
  }, [searchText]);

  const renderTableBody = (requestShape: RequestShape) => {
    return (
      <tr
        key={requestShape.id}
        className='bg-white lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0'
      >
        <td className='w-full lg:w-auto p-3 text-gray-800 text-center border border-b block lg:table-cell relative lg:static'>
          <span className='lg:hidden absolute top-0 left-0 bg-blue-200 px-2 py-1 text-xs font-bold uppercase'>Document title</span>
          {requestShape.documentTitle}
        </td>
        <td className='w-full lg:w-auto p-3 text-gray-800 text-center border border-b text-center block lg:table-cell relative lg:static'>
          <span className='lg:hidden absolute top-0 left-0 bg-blue-200 px-2 py-1 text-xs font-bold uppercase'>Submitted By</span>
          {requestShape.requestBy}
        </td>
        <td className='w-full lg:w-auto p-3 text-gray-800 text-center border border-b text-center block lg:table-cell relative lg:static'>
          <span className='lg:hidden absolute top-0 left-0 bg-blue-200 px-2 py-1 text-xs font-bold uppercase'>Submitted Date</span>
          {/* <span className='rounded bg-red-400 py-1 px-3 text-xs font-bold'>deleted</span> */}
          {requestShape.requestDate}
        </td>
        <td className='w-full lg:w-auto p-3 text-gray-800 text-center border border-b text-center block lg:table-cell relative lg:static'>
          <span className='lg:hidden absolute top-0 left-0 bg-blue-200 px-2 py-1 text-xs font-bold uppercase'>Approved By</span>
          {requestShape.approve}
        </td>
        <td className='w-full lg:w-auto p-3 text-gray-800 text-center border border-b text-center block lg:table-cell relative lg:static'>
          <span className='lg:hidden absolute top-0 left-0 bg-blue-200 px-2 py-1 text-xs font-bold uppercase'>Approved Date</span>
          {requestShape.completedDate}
        </td>
        <td className='w-full lg:w-auto p-3 text-gray-800 text-center border border-b text-center block lg:table-cell relative lg:static'>
          <span className='lg:hidden absolute top-0 left-0 bg-blue-200 px-2 py-1 text-xs font-bold uppercase'>Status</span>
          {requestShape.statusName}
        </td>
        <td className='w-full lg:w-auto p-3 text-gray-800 text-center border border-b text-center block lg:table-cell relative lg:static'>
          <span className='lg:hidden absolute top-0 left-0 bg-blue-200 px-2 py-1 text-xs font-bold uppercase'>Action</span>
          {/* <a href='#' className='text-blue-400 hover:text-blue-600 underline'>
          Edit
        </a>
        <a href='#' className='text-blue-400 hover:text-blue-600 underline pl-6'>
          Remove
        </a> */}
        </td>
      </tr>
    );
  };

  return (
    <div className={styles.root}>
      {/* <button onClick={() => modal.current.open()}>Click</button> */}
      {/* <button onClick={openToast}>Click Toast</button> */}
      {/* <Modal ref={modal}>Hello</Modal> */}
      <div>
        {isModalOpen ? (
          <div ref={ref}>👋 Hey, I'm a modal. Click anywhere outside of me to close.</div>
        ) : (
          <button onClick={() => setModalOpen(true)}>Open Modal</button>
        )}
      </div>
      <div className='w-full max-w-lg'>
        <div key={'1234567'} className='flex flex-wrap -mx-3 mb-2'>
          <div className='w-full md:w-1/3 px-3 mb-6 md:mb-0'>
            <label className='block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2' htmlFor='grid-city'>
              City
            </label>
            <input
              className='appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500'
              type='search'
              onInput={updateSearchText}
              placeholder='Albuquerque'
            />
          </div>
          <div className='w-full md:w-1/3 px-3 mb-6 md:mb-0'>
            <label className='block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2' htmlFor='grid-state'>
              State
            </label>
            <div className='relative'>
              <select
                className='block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500'
                id='grid-state'
              >
                <option>New Mexico</option>
                <option>Missouri</option>
                <option>Texas</option>
              </select>
              <div className='pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700'>
                <svg className='fill-current h-4 w-4' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'>
                  <path d='M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z' />
                </svg>
              </div>
            </div>
          </div>
        </div>
      </div>
      <table className='border-collapse w-full'>
        <thead>
          <tr>
            <th className='p-3 font-bold uppercase bg-gray-200 text-gray-600 border border-gray-300 hidden lg:table-cell'>
              Document title
            </th>
            <th className='p-3 font-bold uppercase bg-gray-200 text-gray-600 border border-gray-300 hidden lg:table-cell'>Submitted By</th>
            <th className='p-3 font-bold uppercase bg-gray-200 text-gray-600 border border-gray-300 hidden lg:table-cell'>
              Submitted Date
            </th>
            <th className='p-3 font-bold uppercase bg-gray-200 text-gray-600 border border-gray-300 hidden lg:table-cell'>Approved By</th>
            <th className='p-3 font-bold uppercase bg-gray-200 text-gray-600 border border-gray-300 hidden lg:table-cell'>Approved Date</th>
            <th className='p-3 font-bold uppercase bg-gray-200 text-gray-600 border border-gray-300 hidden lg:table-cell'>Status</th>
            <th className='p-3 font-bold uppercase bg-gray-200 text-gray-600 border border-gray-300 hidden lg:table-cell'></th>
          </tr>
        </thead>
        <tbody>{dataTable.map(renderTableBody)}</tbody>
      </table>
    </div>
  );
}

export default HomePage;
