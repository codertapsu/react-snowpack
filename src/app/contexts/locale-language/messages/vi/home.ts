export const HomePageLang = {
  greeting: `Xin chào {name}! Bạn ổn chứ?`,
  time: 'The time is {t, time, short}.',
  date: 'The date is {d, date}.',
  myMessage: `Aujourd'hui, c'est le {ts, date, ::yyyyMMdd}`,
};
