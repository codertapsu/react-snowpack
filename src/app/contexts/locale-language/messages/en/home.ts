export const HomePageLang = {
  greeting: 'Hello {name}! How are you?',
  time: 'The time is {t, time, short}.',
  date: 'The date is {d, date}.',
  myMessage: `Aujourd'hui, c'est le {ts, date, ::yyyyMMdd}`,
};
