import { Collection } from '@models/collection.model';

export interface HttpMethod {
  get: <T extends unknown>(path: string, query?: Collection) => Promise<T>;
  post: <T extends unknown>(path: string, body: Collection, query?: Collection) => Promise<T>;
  put: <T extends unknown>(path: string, body?: Collection, query?: Collection) => Promise<T>;
  delete: <T extends unknown>(path: string, query?: Collection) => Promise<T>;
}
