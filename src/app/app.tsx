import { BrowserRouter } from 'react-router-dom';

import { HttpClientProvider } from '@contexts/http-client/http-client.context';
import { LocaleLanguageProvider } from '@contexts/locale-language/locale-language.context';
import { AuthenticationProvider } from '@security/authentication.context';
import { ToastProvider } from '@app-ui/toast/toast.context';

import Router from './routes/routes';

const App = () => {
    return (
      <ToastProvider>
        <LocaleLanguageProvider>
          <AuthenticationProvider>
            <HttpClientProvider>
              <BrowserRouter>
                <Router />
              </BrowserRouter>
            </HttpClientProvider>
          </AuthenticationProvider>
        </LocaleLanguageProvider>
      </ToastProvider>
    );
};

export { App };
