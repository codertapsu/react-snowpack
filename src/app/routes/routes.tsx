import React, { Suspense } from 'react';
import { Redirect, Route, RouteProps, Switch } from 'react-router-dom';

import { useAuthentication } from '@security/authentication.context';

type AsyncRouteProps = RouteProps & { importPath: () => Promise<any> };

const AsyncRoute = ({ importPath, ...props }: AsyncRouteProps) => {
  return <Route {...props} component={React.lazy(importPath)} />;
};

const AuthenticatedRoute = (props: AsyncRouteProps) => {
  const { currentUser } = useAuthentication();
  console.log(currentUser);

  if (!currentUser) {
    return <Redirect to='/sign-in' />;
  }

  return <AsyncRoute {...props} />;
};

export default function Router(): JSX.Element {
  return (
    <Suspense fallback={<div>Loading... </div>}>
      <Switch>
        <AuthenticatedRoute exact path='/' importPath={() => import('../pages/home/home')} />
        <AsyncRoute exact path='/sign-in' importPath={() => import('../modules/sign-in/sign-in')} />
        <AsyncRoute exact path='/sign-up' importPath={() => import('../modules/sign-up/sign-up')} />
      </Switch>
    </Suspense>
  );
}
