import { Collection } from './collection.model';

export interface BaseResponse<D = Collection> {
  data: D;
  failed: false;
  message: null;
  succeeded: true;
}