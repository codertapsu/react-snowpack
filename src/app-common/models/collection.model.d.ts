export interface Collection<T = unknown> {
  [p: string]: T;
}
