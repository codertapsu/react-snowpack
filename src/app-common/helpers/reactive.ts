import { Collection } from '@models/collection.model';

export class Reactive<T extends Collection = Collection> {
  private readonly listeners: Collection<Array<(propChanged: keyof T) => void>> = {};

  public readonly contents: T;

  public constructor(obj: T) {
    this.contents = obj;
    this.makeReactive(obj);
  }

  public listen(prop: keyof T, handler: (propChanged: keyof T) => void): void {
    if (!this.listeners[prop as unknown as string]) {
      this.listeners[prop as unknown as string] = [];
    }
    this.listeners[prop as unknown as string].push(handler);
  }

  private makeReactive(obj: T): void {
    Object.keys(obj).forEach(prop => this.makePropReactive(obj, prop));
  }

  private makePropReactive(obj: T, prop: keyof T): void {
    let value = obj[prop];

    // Gotta be careful with this here
    const that = this;

    Object.defineProperty(obj, prop, {
      get(): T[keyof T] {
        return value;
      },
      set(newValue): void {
        console.log(this, 'this');

        value = newValue;
        that.notify(prop);
      },
    });
  }

  private notify(prop: keyof T): void {
    this.listeners[prop as unknown as string].forEach(listener => listener(this.contents[prop] as unknown as string));
  }
}
const data = new Reactive({
  foo: 'bar',
  foo2: 'bar2',
});

data.listen('foo', change => console.log('Change: ' + change));

data.contents.foo = 'baz';
