import { render } from 'react-dom';

import { App } from './app/app';
import './styles.scss';

// render(<App />, document.getElementById('app-root'));
window.customElements.define(
  'app-root',
  class extends HTMLElement {
    static get observedAttributes() {
      return ['title'];
    }

    public constructor() {
      super();
    }

    public attributeChangedCallback(name: string, oldValue: string, newValue: string): void {
      console.log({oldValue, newValue});
      if (name === 'title') {
        
        // render(this.createCollapsed(newValue), this.mountPoint);
      }
    }

    public connectedCallback(): void {
      render(<App />, this);
    }
  },
);

// window.customElements.define(
//   'app-root',
//   class extends HTMLElement {
//     static get observedAttributes() {
//       return ['title'];
//     }

//     mountPoint: HTMLSpanElement;
//     title: string;

//     public constructor() {
//       super();
//     }

//     // public connectedCallback(): void {
//     //   render(<App />, this);
//     // }

//     public createCollapsed(title: string): FunctionComponentElement<any> {
//       return createElement(App, { title }, createElement('slot'));
//     }

//     public connectedCallback(): void {
//       this.mountPoint = document.createElement('span');
//       const shadowRoot = this.attachShadow({ mode: 'open' });
//       shadowRoot.appendChild(this.mountPoint);

//       const title = this.getAttribute('title');
//       render(this.createCollapsed(title), this.mountPoint);
//       // retargetEvents(shadowRoot);
//     }

//     public attributeChangedCallback(name: string, oldValue: string, newValue: string): void {
//       if (name === 'title') {
//         render(this.createCollapsed(newValue), this.mountPoint);
//       }
//     }
//   },
// );
