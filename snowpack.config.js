// Snowpack Configuration File
// See all supported options: https://www.snowpack.dev/reference/configuration

/** @type {import("snowpack").SnowpackUserConfig } */
module.exports = {
  exclude: [
    "**/node_modules/**/*"
  ],
  mount: {
    public: { url: "/", static: true },
    src: "/",
  },
  plugins: [
    [
      '@snowpack/plugin-sass',
      {
        compilerOptions: {
          style: "compressed",
          sourceMap: false,
          loadPath: ['./node_modules', './src/app-common/scss']
        }
      },
    ],
    ['@snowpack/plugin-react-refresh', { babel: false }],
    '@snowpack/plugin-dotenv',
    '@snowpack/plugin-babel',
    /* ... */
  ],
  routes: [
    {
      match: 'routes',
      src: '.*',
      dest: '/index.html',
    },
  ],
  packageOptions: {
    knownEntrypoints: [
      'react/jsx-runtime'
   ],
    /* ... */
  },
  devOptions: {
    port: 8080
    /* ... */
  },
  buildOptions: {
    /* ... */
  },
  alias: {
    "@app-ui": "./src/app-ui",
    "@contexts": "./src/app/contexts",
    "@helpers": "./src/app-common/helpers",
    "@hook": "./src/app-common/hook",
    "@models": "./src/app-common/models",
    "@security": "./src/app/security",
  },
};
