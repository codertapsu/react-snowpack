# Pull official base image
FROM node:14-alpine

# Set working directory
WORKDIR /app

# Add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# Install app dependencies
COPY package.json ./
COPY package-lock.json ./
RUN npm install --silent

# Add app
COPY . .

# Set port
EXPOSE 8080

# Start app
CMD ["npm", "run", "start"]