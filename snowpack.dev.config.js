const snowpackConfig = require('./snowpack.config');

module.exports = {
  ...snowpackConfig,
  mode: 'development',
};
