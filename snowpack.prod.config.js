const snowpackConfig = require('./snowpack.config');

module.exports = {
  ...snowpackConfig,
  mode: 'production',
  optimize: {
    bundle: true,
    minify: true,
    sourcemap: false,
    splitting: true,
    target: 'es2018',
    treeshake: true,
  }
};
